" Handle a :write operation; prompt for directory creation if needed with
" 'confirm', force it with :write!
function! write_mkpath#(path) abort

  " Get all directory elements leading up to directory
  let dir = fnamemodify(a:path, ':h')

  " Directory exists, we don't need to do anything
  if isdirectory(dir)
    return
  endif

  " If :write! was issued, we'll try to create the missing path; failing that,
  " if 'confirm' is enabled, and the user responds affirmatively to the
  " prompt, that will do, too.  Otherwise, we will allow the write to fail.
  if v:cmdbang
    let mkpath = 1
  elseif &confirm
    let mkpath = confirm('Create path '.dir.'?', "&Yes\n&No") == 1
  else
    let mkpath = 0
  endif

  " Stop here if we're not creating a path
  if !mkpath
    return
  endif

  " Create the full required path
  call mkdir(dir, 'p')

  " Prod Vim into realising the buffer's directory exists now, so that a
  " subsequent change of working directory doesn't break it; this doesn't
  " appear to be necessary on Windows, for reasons unclear
  if !has('win32') && !has('win64')
    silent keepalt file %
  endif

  " Re-run the BufWritePre hook, now that the directory exists and a useable
  " filename has been set; this will start this function again from the top,
  " but stop when it sees the directory now exists
  doautocmd write_mkpath BufWritePre

endfunction
