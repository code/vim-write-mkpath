write\_mkpath.vim
=================

This plugin supports creation of any missing path elements in the path to a
buffer's filename before the buffer is written.

If `'confirm'` is enabled on `:write` or `:w`, the user will be prompted to
confirm that Vim should create any missing path elements.

On `:write!` or `:w!`, the path will be created automatically, ignoring the
value of `'confirm'`.

Inspired by Damian Conway's [`automkdir.vim` plugin][1].

License
-------

Copyright (c) [Tom Ryder][2].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://github.com/thoughtstream/Damian-Conway-s-Vim-Setup/blob/cbe1fb5/plugin/automkdir.vim
[2]: https://sanctum.geek.nz/
