"
" write_mkpath: Create any missing path elements in the path to a buffer's
" filename before the buffer is written.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_write_mkpath') || &compatible || v:version < 702
  finish
endif

" Check path to every file before it's saved
augroup write_mkpath
  autocmd!
  autocmd BufWritePre *
        \ call write_mkpath#(expand('<afile>'))
augroup END
